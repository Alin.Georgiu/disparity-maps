#include "stdafx.h"

#include "common.h"
#include <CommDlg.h>
#include <ShlObj.h>

FileGetter::FileGetter(char* folderin,char* ext){		
	strcpy(folder,folderin);
	char folderstar[MAX_PATH];	
	if( !ext ) strcpy(ext,"*");
	sprintf(folderstar,"%s\\*.%s",folder,ext);
	hfind = FindFirstFileA(folderstar,&found);
	hasFiles= !(hfind == INVALID_HANDLE_VALUE);
	first = 1;
	//skip .
	//FindNextFileA(hfind,&found);		
}

int FileGetter::getNextFile(char* fname){
	if (!hasFiles)
		return 0;
	//skips .. when called for the first time
	if( first )
	{
		strcpy(fname, found.cFileName);				
		first = 0;
		return 1;
	}
	else{
		chk=FindNextFileA(hfind,&found);
		if (chk)
			strcpy(fname, found.cFileName);				
		return chk;
	}
}

int FileGetter::getNextAbsFile(char* fname){
	if (!hasFiles)
		return 0;
	//skips .. when called for the first time
	if( first )
	{
		sprintf(fname, "%s\\%s", folder, found.cFileName);			
		first = 0;
		return 1;
	}
	else{
		chk=FindNextFileA(hfind,&found);
		if (chk)
			sprintf(fname, "%s\\%s", folder, found.cFileName);				
		return chk;
	}
}

char* FileGetter::getFoundFileName(){
	if (!hasFiles)
		return 0;
	return found.cFileName;
}

LPCWSTR convertLPCWSTR(const char* input)
{
	wchar_t wtext[255];
	mbstowcs(wtext, input, strlen(input) + 1);//Plus null
	LPCWSTR ptr = wtext;

	return ptr;
}
LPWSTR convertLPWSTR(const char* input)
{
	wchar_t wtext[255];
	mbstowcs(wtext, input, strlen(input) + 1);//Plus null
	LPWSTR ptr = wtext;

	return ptr;
}

char* convertBack(LPWSTR input)
{
	char* resultStr = new char[wcslen(input) + 1];
	wsprintfA(resultStr, "%S", input);
	return resultStr;
}


int openFileDlg(char* fname, const char* title)
{
	HWND owner = NULL;
	OPENFILENAME ofn;
	char fileName[MAX_PATH];
	strcpy(fileName,"");
	ZeroMemory(&ofn, sizeof(ofn));
	ofn.lStructSize = sizeof(OPENFILENAME);
	ofn.lpstrTitle = convertLPWSTR(title);;
	ofn.hwndOwner = owner;
	ofn.lpstrFile = convertLPWSTR(fileName);
	ofn.nMaxFile = MAX_PATH;
	ofn.Flags = OFN_EXPLORER | OFN_FILEMUSTEXIST | OFN_HIDEREADONLY;
	ofn.lpstrDefExt = convertLPCWSTR("");
	GetOpenFileName(&ofn); 
	strcpy(fname, convertBack(ofn.lpstrFile));
	return strcmp(fname,"");
}

int openFolderDlg(const char *folderName)
{
	BROWSEINFO bi;
	ZeroMemory(&bi, sizeof(bi));
	SHGetPathFromIDList(SHBrowseForFolder(&bi),convertLPWSTR(folderName));
	return strcmp(folderName,"");
}

void resizeImg(Mat src, Mat &dst, int maxSize, bool interpolate)
{
	double ratio = 1;
	double w = src.cols;
	double h = src.rows;
	if (w>h)
		ratio = w/(double)maxSize;
	else
		ratio = h/(double)maxSize;
	int nw = (int) (w / ratio);
	int nh = (int) (h / ratio);
	Size sz(nw,nh);
	if (interpolate)
		resize(src,dst,sz);
	else
		resize(src,dst,sz,0,0,INTER_NEAREST);
}
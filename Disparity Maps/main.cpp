//
// OpenCVApplication.cpp : Defines the entry point for the console application.
//
#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include "common.h"
#include <iostream>

using namespace std;
using namespace cv;

//SSD + SAD macros
#define KERNEL_SIZE 7
#define MAX_DISPARITY 30

//RGB -> Grayscale macros
#define RED_WEIGHT 0.299
#define BLUE_WEIGHT 0.114
#define GREEN_WEIGHT 0.587

//CSCA macros
#define PYRAMID_LEVELS 5

//CSCA cost-computation macros
#define TAU_1 0.02745
#define TAU_2 0.00784
#define ALPHA 0.11

//CSCA cost-aggregation macros

#define BILLATERAL_FILTER_KERNEL_SIZE 15
#define COST_ALPHA 0.75

//CSCA post-processing macro
#define MEDIAN_FILTER_SIZE 3

const char* outputPath = "OutputMap.png";

//CSCA Gaussian Pyramid structure
struct CostPyramid {
	Mat_<double>* costVol;
	int height;
	int width;
	int maxDisparity;
	int disparityScale; //Middlebury specific scale factor
};

//Helper functions
bool isInside(Mat img, int i, int j)
{
	if (i < 0 || j < 0)
		return false;
	else
		if (i >= img.rows || j >= img.cols)
			return false;
		else
			return true;
}

void displayDoubleImage(const char* title, Mat_<double> input)
{
	Mat output;
	normalize(input, output, 0, 255, NORM_MINMAX, CV_8UC1);
	imshow(title, output);
}

Mat_<uchar> convert_BGR_To_Grey(Mat_<Vec3b> input)
{
	Mat_<uchar> output(input.rows, input.cols);
	for (int i = 0; i < input.rows; i++)
		for (int j = 0; j < input.cols; j++)
			output(i, j) = input(i, j)[0] * BLUE_WEIGHT + input(i, j)[1] * GREEN_WEIGHT + input(i, j)[2] * RED_WEIGHT;
	return output;
}

double distance(double p1, double p2)
{
	return fabs(p1 - p2);
}

// SSD + SAD methods
int SSD(int a, int b)
{
	return pow(a - b, 2);
}

int SAD(int a, int b) {
	return abs(a - b);
}

Mat_<uchar> calculateDepth(const char* windowName, Mat_<Vec3b> leftImg, Mat_<Vec3b> rightImg, int (*method)(int, int), int kernel_size)
{
	int height = leftImg.rows;
	int width = leftImg.cols;

	int half = kernel_size / 2;

	int adjustment = 255 / MAX_DISPARITY;

	Mat_<uchar> leftTemp = convert_BGR_To_Grey(leftImg);
	Mat_<uchar> rightTemp = convert_BGR_To_Grey(rightImg);

	Mat_<uchar> output(height, width);

	for (int x = 0; x < height; x++)
	{
		printf("%d out of %d iterations\n", x, height - 1);

		for (int y = 0; y < width; y++)
		{
			int bestDisparity = 0;
			int prevSSD = INT_MAX;

			for (int disparity = 0; disparity < MAX_DISPARITY; disparity++)
			{
				int ssd = 0;
				for (int v = -half; v < half; v++)
					for (int u = -half; u < half; u++)
						if (isInside(leftTemp, x + v, y + u) && isInside(rightTemp, x + v, y + u - disparity))
							ssd += method(leftTemp(x + v, y + u), rightTemp(x + v, y + u - disparity));

				if (ssd < prevSSD)
				{
					prevSSD = ssd;
					bestDisparity = disparity;
				}
			}
			output(x, y) = bestDisparity * adjustment;
		}
	}
	imshow(windowName, output);

	return output;
}


// CSCA methods
double costGradient(Vec3d leftPixel, Vec3d rightPixel, double leftGradient, double rightGradient)
{
	double colorDifference = 0;
	double gradientDifference = 0;

	for (int i = 0; i < 3; i++)
		colorDifference += distance(leftPixel[i], rightPixel[i]);

	colorDifference *= (1.0 / 3.0);

	gradientDifference = distance(leftGradient, rightGradient);

	if (colorDifference > TAU_1)
		colorDifference = TAU_1;

	if (gradientDifference > TAU_2)
		gradientDifference = TAU_2;

	return ALPHA * colorDifference + (1 - ALPHA) * gradientDifference;
}

double costGradient(Vec3b leftPixel, double leftGradient)
{
	double colorDifference = 0;
	double gradientDifference = 0;

	for (int i = 0; i < 3; i++)
		colorDifference += fabs(leftPixel[i]);

	colorDifference *= (1.0 / 3.0);

	gradientDifference = fabs(leftGradient);

	if (colorDifference > TAU_1)
		colorDifference = TAU_1;

	if (gradientDifference > TAU_2)
		gradientDifference = TAU_2;

	return ALPHA * colorDifference + (1 - ALPHA) * gradientDifference;
}
void IntensityGradient(Mat_<Vec3d> lImg, Mat_<Vec3d> rImg, int maxDisparity, Mat_<double>* costVol)
{
	int height = lImg.rows;
	int width = lImg.cols;

	Mat_<float> leftTemp;
	Mat_<float> rightTemp;
	Mat tmp;

	Mat_<double> leftGradientX, rightGradientX;

	lImg.convertTo(tmp, CV_32F);
	cvtColor(tmp, leftTemp, COLOR_RGB2GRAY);
	rImg.convertTo(tmp, CV_32F);
	cvtColor(tmp, rightTemp, COLOR_RGB2GRAY);

	// X Gradient
	Sobel(leftTemp, leftGradientX, CV_64F, 1, 0, 1);
	Sobel(rightTemp, rightGradientX, CV_64F, 1, 0, 1);
	leftGradientX += 0.5;
	rightGradientX += 0.5;

	for (int d = 0; d < maxDisparity; d++)
		for (int y = 0; y < height; y++)
			for (int x = 0; x < width; x++)
				if (x - d >= 0) {
					Vec3d leftPixel = lImg(y, x);
					Vec3d rightPixel = rImg(y, x - d);
					double leftGradValue = leftGradientX(y, x);
					double rightGradValue = rightGradientX(y, x - d);
					costVol[d](y, x) = costGradient(leftPixel, rightPixel, leftGradValue, rightGradValue);
				}
				else {
					Vec3d leftPixel = lImg(y, x);
					double leftGradValue = leftGradientX(y, x);
					costVol[d](y, x) = costGradient(leftPixel, leftGradValue);
				}
}
Mat_<double> BillateralFilter(Mat_<double> input, Mat_<double> cost, int kernelSize, double sigmaS, double sigmaR)
{
	int height = input.rows;
	int width = input.cols;
	int half = kernelSize / 2;

	Mat_<double> dest = cost.clone();

	for (int y = 0; y < height; y++) {
		for (int x = 0; x < width; x++) {
			Vec3d kernelCenterPixel = input(y, x);
			double sum = 0.0;
			double weightsSum = 0.0;
			for (int v = -half; v <= half; v++) {
				for (int u = -half; u <= half; u++) {
					int offsetX = x + u;
					int offsetY = y + v;
					if (isInside(cost, offsetY, offsetX) && isInside(input, offsetY, offsetX))
					{
						Vec3d kernelOffsetPixel = input(offsetY, offsetX);
						double spDis = u * u + v * v;
						double clrDis = 0.0;

						for (int c = 0; c < 3; c++)
							clrDis += distance(kernelCenterPixel[c], kernelOffsetPixel[c]);

						clrDis *= (1.0 / 3.0);
						double weight = exp(-spDis / (sigmaS * sigmaS) - clrDis * clrDis / (sigmaR * sigmaR));

						sum += weight * cost(offsetY, offsetX);
						weightsSum += weight;
					}
				}
			}
			if (isInside(dest, y, x))
				dest(y, x) = sum / weightsSum;
		}
	}
	return dest;
}
void addCostVolume(CostPyramid* prevPyramid, CostPyramid* currPyramid, int maxDis)
{
	for (int d = 1; d < currPyramid->maxDisparity; d++)
	{
		int pD = (d + 1) / 2;
		for (int y = 0; y < currPyramid->height; y++)
		{
			int pY = y / 2;
			for (int x = 0; x < currPyramid->width; x++)
			{
				int pX = x / 2;
				if (isInside(prevPyramid->costVol[pD], pY, pX) && isInside(currPyramid->costVol[d], y, x))
					currPyramid->costVol[d](y, x) = currPyramid->costVol[d](y, x) * COST_ALPHA + prevPyramid->costVol[pD](pY, pX) * (1 - COST_ALPHA);
			}
		}
	}
}

void aggregateCost(CostPyramid** pyramid)
{
	for (int p = PYRAMID_LEVELS - 2; p >= 0; p--)
	{
		printf("Processed pyramid: %d\n", p);
		addCostVolume(pyramid[p + 1], pyramid[p], pyramid[p]->maxDisparity);
	}
}
Mat_<uchar> match(CostPyramid* pyramid)
{
	Mat_<double>* costVol = pyramid->costVol;
	int height = pyramid->height;
	int width = pyramid->width;

	Mat_<uchar> disparityMap(height, width);

	for (int y = 0; y < height; y++)
		for (int x = 0; x < width; x++)
		{
			double minCost = DBL_MAX;
			int bestDisparity = 0;

			for (int d = 1; d < pyramid->maxDisparity; d++)
			{
				if (isInside(costVol[d], y, x))
					if (costVol[d](y, x) < minCost)
					{
						minCost = costVol[d](y, x);
						bestDisparity = d;
					}
			}
			disparityMap(y, x) = bestDisparity * pyramid->disparityScale;
		}

	Mat_<uchar> output;
	normalize(disparityMap, output, 0, 255, NORM_MINMAX, CV_8UC1);
	return output;
}

Mat_<uchar> matchSinglePicture(Mat_<double>* costVol, int maxDisparity, int disSc, int height, int width, int index)
{
	Mat_<uchar> disparityMap(height, width);

	for (int y = 0; y < height; y++)
		for (int x = 0; x < width; x++)
		{
			double minCost = DBL_MAX;
			int minDis = 0;

			for (int d = 1; d < maxDisparity; d++)
			{
				if (isInside(costVol[d], y, x))
					if (costVol[d](y, x) < minCost)
					{
						minCost = costVol[d](y, x);
						minDis = d;
					}
			}
			disparityMap(y, x) = minDis * disSc;
		}

	/*Mat output;
	normalize(disparityMap, output, 0, 255, NORM_MINMAX, CV_8UC1);
	imshow("Output Map" + index, output);*/

	return disparityMap;
}

Mat_<uchar> CrossScaleStereo(Mat_<Vec3d> leftImg, Mat_<Vec3d> rightImg, int billateralFilterKernel, int medianFilterKernel) {

	int maxDisparity = 60;
	int disparityScale = 8;

	int maxDisparityCopy = maxDisparity;

	CostPyramid** pyramid = new CostPyramid * [PYRAMID_LEVELS];

	Mat_<Vec3d> leftClone = leftImg.clone();
	Mat_<Vec3d> rightClone = rightImg.clone();

	for (int p = 0; p < PYRAMID_LEVELS; p++)
	{
		pyramid[p] = new CostPyramid;
		pyramid[p]->costVol = new Mat_<double>;

		Mat_<double>* costVol = new Mat_<double>[maxDisparityCopy];

		int height = leftClone.rows;
		int width = leftClone.cols;

		printf("-----------PYRAMID %d-----------\n", p + 1);

		for (int i = 0; i < maxDisparityCopy; i++)
			costVol[i] = Mat::zeros(height, width, CV_64FC1);

		printf("Cost computation...\n");
		IntensityGradient(leftClone, rightClone, maxDisparityCopy, costVol);

		printf("Filtering operation...\n");
		for (int i = 0; i < maxDisparityCopy; i++)
		{
			printf("Applying filter for picture %d\n", i);
			costVol[i] = BillateralFilter(leftClone, costVol[i], billateralFilterKernel, 5, 0.028);
		}

		pyramid[p]->costVol = costVol;
		pyramid[p]->height = height;
		pyramid[p]->width = width;
		pyramid[p]->maxDisparity = maxDisparityCopy;
		pyramid[p]->disparityScale = disparityScale;

		maxDisparityCopy = maxDisparityCopy / 2 + 1;
		disparityScale *= 2;

		pyrDown(leftClone, leftClone);
		pyrDown(rightClone, rightClone);
	}

	printf("Adding all the costs...\n");
	aggregateCost(pyramid);

	printf("Matching...\n");
	Mat_<uchar> result = match(pyramid[0]);
	Mat_<uchar> output;

	printf("Applying post-processing blur...\n");
	medianBlur(result, output, medianFilterKernel);
	printf("Done!\n");

	imshow("Depth map", output);
	return output;
}

float calculateAccuracy(Mat_<uchar> disparity, Mat_<uchar> groundTruth, double errorRate)
{
	int badPixels = 0;

	float total = disparity.rows * disparity.cols;
	for (int x = 0; x < disparity.rows; x++)
		for (int y = 0; y < disparity.cols; y++)
			if (abs(disparity(x, y) * 1.0 - (groundTruth(x, y) * 1.0)) > errorRate)
				badPixels++;

	float accuracy = ((float)badPixels / total);

	printf("Accuracy = %f\n", 1.0 - accuracy);
	return (1.0-accuracy);
}

Mat openImage(const char * title) {
	Mat input;
	char fName[_MAX_PATH];
	while (openFileDlg(fName, title))
	{
		input = imread(fName);
		if (fName)
			break;
	}
	return input;
}

int main()
{

	int operation;

	do {
		printf("Menu:\n");
		printf("1 - Apply SSD on a pair of stereo images\n");
		printf("2 - Apply SAD on a pair of stereo images\n");
		printf("3 - Apply CSCA on a pair of stereo images\n");
		printf("0 - Exit\n\n");

		Mat leftImg, rightImg, groundTruth, resultAlg1, resultAlg2;

		const char* leftImgTitle = "Left Image", * rightImgTitle = "Right Image", *groundTruthTitle = "Groundtruth";

		char v = '0';

		int billateralFilterKernel = BILLATERAL_FILTER_KERNEL_SIZE;
		int medianFilterKernel = MEDIAN_FILTER_SIZE;
		int SSDSADKernel = KERNEL_SIZE;

		printf("Select an operation: ");
		cin >> operation;

		switch (operation)
		{
		case 1: 
			leftImg = openImage(leftImgTitle);
			rightImg = openImage(rightImgTitle);
			groundTruth = openImage(groundTruthTitle);
			if (leftImg.empty() || rightImg.empty() || groundTruth.empty())
			{
				printf("Operation encountered a problem, abort\n");
				break;
			}

			printf("Do you want to customize the kernel size? [y/N]\n");
			cin >> v;

			if (v == 'y') {
				printf("Kernel Size= ");
				cin >> SSDSADKernel;
			}

			imshow("Left image", leftImg);
			imshow("Right image", rightImg);
			imshow("Groundtruth", groundTruth);

			resultAlg1 = calculateDepth("SSD", leftImg, rightImg, SSD, SSDSADKernel);
			calculateAccuracy(resultAlg1, groundTruth, 1.0);
			waitKey();

			break;

		case 2:
			leftImg = openImage(leftImgTitle);
			rightImg = openImage(rightImgTitle);
			groundTruth = openImage(groundTruthTitle);
			if (leftImg.empty() || rightImg.empty() || groundTruth.empty())
			{
				printf("Operation encountered a problem, abort\n");
				break;
			}

			printf("Do you want to customize the kernel size? [y/N]\n");
			cin >> v;

			if (v == 'y') {
				printf("Kernel Size= ");
				cin >> SSDSADKernel;
			}
			imshow("Left image", leftImg);
			imshow("Right image", rightImg);
			imshow("Groundtruth", groundTruth);

			resultAlg1 = calculateDepth("SAD", leftImg, rightImg, SAD, SSDSADKernel);

			calculateAccuracy(resultAlg1, groundTruth, 3.0);
			waitKey();

			break;
		case 3:
			leftImg = openImage(leftImgTitle);
			rightImg = openImage(rightImgTitle);
			groundTruth = openImage(groundTruthTitle);

			leftImg.convertTo(leftImg, CV_64F, 1 / 255.0f);
			rightImg.convertTo(rightImg, CV_64F, 1 / 255.0f);


			if (leftImg.empty() || rightImg.empty() || groundTruth.empty())
			{
				printf("Operation encountered a problem, abort\n");
				break;
			}
			imshow("Left image", leftImg);
			imshow("Right image", rightImg);
			imshow("Groundtruth", groundTruth);

			printf("Do you want to customize the parameters? [y/N]\n");
			cin >> v;

			if (v == 'y') {
				printf("Bilateral Filter Kernel Size = ");
				cin >> billateralFilterKernel;
				printf("Median Filter Kernel Size = ");
				cin >> medianFilterKernel;
			}

			resultAlg2 = CrossScaleStereo(leftImg, rightImg, billateralFilterKernel, medianFilterKernel);

			imwrite(outputPath, resultAlg2);

			calculateAccuracy(resultAlg2, groundTruth, 3.0);
			waitKey();

			break;
			default:
				printf("Try again\n");
			break;
		}
	} while (operation != 0);

	return 0;
}
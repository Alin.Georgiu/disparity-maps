
# Stereo Vision Disparity Mapping

An advanced image processing application for generating high-quality disparity maps from stereo image pairs using SSD/SAD and CSCA algorithms.

## Overview

This project implements and analyzes classical computer vision approaches to disparity map generation, focusing on computational efficiency and accuracy without relying on machine learning techniques. The system processes stereo image pairs to create detailed disparity maps, which are essential for depth perception in computer vision applications.

## Key Features

### Algorithm Implementation
- **SSD (Sum of Squared Differences)**
  - Pixel-wise squared difference calculation
  - Efficient window-based matching
  - Optimized computational performance

- **SAD (Sum of Absolute Differences)**
  - Lower computational complexity alternative
  - Robust to outliers
  - Memory-efficient implementation

- **CSCA (Cost Aggregation at Different Resolution Levels)**
  - Multi-resolution analysis
  - Adaptive cost aggregation
  - Enhanced accuracy in challenging scenarios

### Performance Analysis
- Quantitative accuracy measurements
- Comparison with ground truth disparity maps
- Performance benchmarking tools
- Detailed error analysis and visualization

## Technical Details

### Dependencies
- OpenCV (4.x or higher)
- C++ 17 compiler
- CMake (3.10+)
- Boost Libraries (optional, for advanced features)

### Building the Project

```bash
mkdir build
cd build
cmake ..
make
```

### Usage

1. Basic disparity map generation:
```bash
./disparity_map --left left_image.png --right right_image.png --method SAD
```

2. Advanced options:
```bash
./disparity_map --left left_image.png --right right_image.png \
                --method CSCA \
                --window_size 9 \
                --max_disparity 64 \
                --scale_factor 0.5
```

## Algorithm Details

### SSD/SAD Implementation
- Window-based matching approach
- Configurable window sizes (default: 9x9)
- Optimized sliding window computation
- Sub-pixel interpolation for enhanced accuracy

### CSCA Method
1. Multi-resolution pyramid generation
2. Cost volume computation
3. Adaptive cost aggregation
4. Cross-scale consistency checking
5. Refinement and post-processing

## Performance Metrics

The system evaluates disparity maps using:
- Percentage of correctly matched pixels
- Root Mean Square Error (RMSE)
- Bad pixel percentage (threshold-based)
- Processing time measurements

## Results Visualization

The project includes tools for:
- Disparity map visualization
- Error map generation
- Cross-sectional analysis
- 3D point cloud visualization




